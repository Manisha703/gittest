/*-----------------------------------------------------------------------------
    Name: exit_script
    Recorded By: shubham
    Date of recording: 12/22/2021 03:32:47
    Flow details:
    Build details: 4.6.1 (build# 90)
    Modification History:
-----------------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "ns_string.h"

int exit_script()
{
    return 0;
}
